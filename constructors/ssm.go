package constructors

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ssm"
	"gitlab.com/marmll/warp/interfaces/log"
	"go.uber.org/fx"
)

type SSMGetParameterApi interface {
	GetParameter(ctx context.Context,
		params *ssm.GetParameterInput,
		optFns ...func(*ssm.Options)) (*ssm.GetParameterOutput, error)
}

func findParameter(c context.Context, api SSMGetParameterApi, input *ssm.GetParameterInput) (*ssm.GetParameterOutput, error) {
	return api.GetParameter(c, input)
}

type SSMClient struct {
	Client SSMFunctions
}

type SSMFunctions interface {
	GetParameter(context.Context, *string) (string, error)
}

type ssmDeps struct {
	fx.In

	LifeCycle fx.Lifecycle
	Logger    log.Logger
}

type ssmImpl struct {
	deps   ssmDeps
	client *ssm.Client
}

func (s *ssmImpl) GetParameter(ctx context.Context, s2 *string) (string, error) {
	input := &ssm.GetParameterInput{
		Name: s2,
	}
	results, err := findParameter(ctx, s.client, input)
	if err != nil {
		return "", err
	}
	return *results.Parameter.Value, nil
}

func SSMService(deps ssmDeps) (client *SSMClient, err error) {
	var clientPtr = new(SSMClient)
	var cfg aws.Config

	deps.LifeCycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) (startError error) {
			if cfg, startError = config.LoadDefaultConfig(ctx); startError != nil {
				deps.Logger.WithError(startError).Error(ctx, "failed to aws load config")
				return
			}
			clientPtr.Client = &ssmImpl{
				deps:   deps,
				client: ssm.NewFromConfig(cfg),
			}
			return
		},
		OnStop: func(ctx context.Context) (stopError error) {
			return
		},
	})

	client = clientPtr
	return
}
