package constructors

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/marmll/warp/interfaces/cfg"
	"gitlab.com/marmll/warp/interfaces/log"
	warp "gitlab.com/marmll/warp/links/redis"
	"go.uber.org/fx"
	"time"
)

type RedisClient struct {
	Client RedisFunctions
}

type RedisFunctions interface {
	Disconnect() error
	Ping() bool
	Add(key string, value string) error
	Get(key string) (string, error)
	Flush() error
}

type redisDeps struct {
	fx.In

	Config    cfg.Config
	LifeCycle fx.Lifecycle
	Logger    log.Logger
}

type redisClientImpl struct {
	deps   redisDeps
	client *redis.Client
}

const (
	addrLocal = ":6379"
)

func RedisService(deps redisDeps) (client *RedisClient, err error) {
	var clientPtr = new(RedisClient)
	deps.LifeCycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) (startError error) {
			var redisAddress = addrLocal

			redisServer := deps.Config.Get("server.redis.server").String()
			redisPort := deps.Config.Get("server.redis.port").String()
			if redisServer == "" {
				deps.Logger.Info(ctx, "Using default redis server %s", redisAddress)
			} else {
				deps.Logger.Info(ctx, "Using redis server %s with port %s", redisServer, redisPort)
				redisAddress = redisServer + ":" + redisPort
			}

			rdb := redis.NewClient(&redis.Options{
				Addr:     redisAddress,
				Password: "",
				DB:       0, // customer-identity 1: customer-contract
			})
			if _, err := rdb.Ping(ctx).Result(); err != nil {
				time.Sleep(3 * time.Second)
				err := rdb.Ping(ctx).Err()
				if err != nil {
					panic("Unable to connect to redis " + err.Error())
				}
			}
			clientPtr.Client = &redisClientImpl{
				deps:   deps,
				client: rdb,
			}
			return
		},
		OnStop: func(ctx context.Context) (stopError error) {
			if clientPtr.Client != nil {
				if stopError = clientPtr.Client.Disconnect(); stopError != nil {
					deps.Logger.WithError(stopError).Error(ctx, "failed to disconnect from redis db")
				}
			}
			return
		},
	})
	client = clientPtr
	return
}

func (impl *redisClientImpl) Disconnect() error {
	return impl.client.Close()
}

func (impl *redisClientImpl) Ping() bool {
	impl.deps.Logger.Debug(nil, "Warp Redis Ping")
	var result bool = false

	if _, err := impl.client.Ping(context.Background()).Result(); err != nil {
		result = false
	} else {
		result = true
	}

	return result
}

func (impl *redisClientImpl) Add(key string, value string) error {
	return warp.RedisSet(context.Background(), impl.client, key, value)
}

func (impl *redisClientImpl) Get(key string) (string, error) {
	return warp.RedisGet(context.Background(), impl.client, key)
}

func (impl *redisClientImpl) Flush() error {
	return warp.RedisFlushAll(context.Background(), impl.client)
}
