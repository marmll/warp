package constructors

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"gitlab.com/marmll/warp/interfaces/log"
	"gitlab.com/marmll/warp/links/dynamo"
	"go.uber.org/fx"
)

type DynamoClient struct {
	Client DynamoFunctions
}

type DynamoFunctions interface {
	Table(table string) dynamo.ClientTable
}

type dynamoDeps struct {
	fx.In

	LifeCycle fx.Lifecycle
	Logger    log.Logger
}

type dynamoImpl struct {
	deps   dynamoDeps
	client *dynamodb.Client
}

func (d *dynamoImpl) Table(table string) dynamo.ClientTable {
	return dynamo.Table(d.client, table)
}

func DynamoService(deps dynamoDeps) (client *DynamoClient, err error) {
	var clientPtr = new(DynamoClient)
	var cfg aws.Config

	deps.LifeCycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) (startError error) {
			if cfg, startError = config.LoadDefaultConfig(ctx); startError != nil {
				deps.Logger.WithError(startError).Error(ctx, "failed to aws load config")
				return
			}
			clientPtr.Client = &dynamoImpl{
				deps:   deps,
				client: dynamodb.NewFromConfig(cfg),
			}
			return
		},
		OnStop: func(ctx context.Context) (stopError error) {
			return
		},
	})

	client = clientPtr
	return
}
