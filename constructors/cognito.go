package constructors

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	cognito "github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider"
	"gitlab.com/marmll/warp/interfaces/log"
	cog "gitlab.com/marmll/warp/links/cognito"
	"go.uber.org/fx"
)

type CognitoClient struct {
	Client CognitoFunctions
}

type CognitoFunctions interface {
	SignUp(clientId string, username string, password string) (result *cognito.SignUpOutput, err error)
}
type cognitoDeps struct {
	fx.In

	LifeCycle fx.Lifecycle
	Logger    log.Logger
}

type cognitoImpl struct {
	deps   cognitoDeps
	client *cognito.Client
}

func (c *cognitoImpl) SignUp(clientId string, username string, password string) (result *cognito.SignUpOutput, err error) {
	return cog.SignUp(c.client, clientId, username, password)
}

func CognitoService(deps cognitoDeps) (client *CognitoClient, err error) {
	var clientPtr = new(CognitoClient)
	var cfg aws.Config

	deps.LifeCycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) (startError error) {
			if cfg, startError = config.LoadDefaultConfig(ctx); startError != nil {
				deps.Logger.WithError(startError).Error(ctx, "failed to aws load config")
				return
			}
			clientPtr.Client = &cognitoImpl{
				deps:   deps,
				client: cognito.NewFromConfig(cfg),
			}
			return
		},
		OnStop: func(ctx context.Context) (stopError error) {
			return
		},
	})
	client = clientPtr
	return
}
