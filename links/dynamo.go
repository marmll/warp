package links

import (
	"gitlab.com/marmll/warp/constructors"
	"go.uber.org/fx"
)

func DynamoFxOptions() fx.Option {
	return fx.Provide(constructors.DynamoService)
}
