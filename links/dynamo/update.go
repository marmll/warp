package dynamo

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"strings"
)

type Update struct {
	table      ClientTable
	returnType types.ReturnValue

	hashKey   string
	hashValue types.AttributeValue

	rangeKey   string
	rangeValue types.AttributeValue

	set    []string
	add    map[string]string
	del    map[string]string
	remove map[string]struct{}

	condition string

	subber

	err error
	cc  *ConsumedCapacity
}

func (table ClientTable) Update(hashKey string, value interface{}) *Update {
	u := &Update{
		table:   table,
		hashKey: hashKey,

		set:    make([]string, 0),
		add:    make(map[string]string),
		del:    make(map[string]string),
		remove: make(map[string]struct{}),
	}
	u.hashValue, u.err = attributevalue.Marshal(value)
	return u
}

type UpdateAPI interface {
	UpdateItem(ctx context.Context,
		params *dynamodb.UpdateItemInput,
		optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error)
}

func updateItem(c context.Context, api UpdateAPI, input *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, error) {
	return api.UpdateItem(c, input)
}

func (u *Update) Range(name string, value interface{}) *Update {
	var err error
	u.rangeKey = name
	u.rangeValue, err = attributevalue.Marshal(value)
	u.setError(err)
	return u
}

func (u *Update) Set(path string, value interface{}) *Update {
	v, err := attributevalue.Marshal(value)
	if v == nil && err == nil {
		return u.Remove(path)
	}
	u.setError(err)

	path, err = u.escape(path)
	u.setError(err)
	expr, err := u.subExpr("🝕 = ?", path, v)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

func (u *Update) SetNullable(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	expr, err := u.subExprN("🝕 = ?", path, value)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

func (u *Update) Remove(paths ...string) *Update {
	for _, n := range paths {
		n, err := u.escape(n)
		u.setError(err)
		u.remove[n] = struct{}{}
	}
	return u
}

// If specifies a conditional expression for this update to succeed.
// Use single quotes to specificy reserved names inline (like 'Count').
// Use the placeholder ? within the expression to substitute values, and use $ for names.
// You need to use quoted or placeholder names when the name is a reserved word in DynamoDB.
// Multiple calls to Update will be combined with AND.
func (u *Update) If(expr string, args ...interface{}) *Update {
	expr = wrapExpr(expr)
	cond, err := u.subExprN(expr, args...)
	u.setError(err)
	if u.condition != "" {
		u.condition += " AND "
	}
	u.condition += cond
	return u
}

// RemoveExpr performs a custom remove expression, substituting the args into expr as in filter expressions.
// 	RemoveExpr("MyList[$]", 5)
func (u *Update) RemoveExpr(expr string, args ...interface{}) *Update {
	expr, err := u.subExpr(expr, args...)
	u.setError(err)
	u.remove[expr] = struct{}{}
	return u
}

func (u *Update) SetSet(path string, value interface{}) *Update {
	v, err := attributevalue.Marshal(value)
	if v == nil && err == nil {
		return u.Remove(path)
	}
	u.setError(err)

	path, err = u.escape(path)
	u.setError(err)
	expr, err := u.subExpr("🝕 = ?", path, v)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

func (u *Update) SetIfNotExists(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	expr, err := u.subExprN("🝕 = if_not_exists(🝕, ?)", path, path, value)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

func (u *Update) SetExpr(expr string, args ...interface{}) *Update {
	expr, err := u.subExprN(expr, args...)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

// Append appends value to the end of the list specified by path.
func (u *Update) Append(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	expr, err := u.subExprN("🝕 = list_append(🝕, ?)", path, path, value)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

// Prepend inserts value to the beginning of the list specified by path.
func (u *Update) Prepend(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	expr, err := u.subExprN("🝕 = list_append(?, 🝕)", path, value, path)
	u.setError(err)
	u.set = append(u.set, expr)
	return u
}

func (u *Update) Value(out interface{}) error {
	return u.ValueWithContext(context.Background(), out)
}

func (u *Update) ValueWithContext(ctx context.Context, out interface{}) error {
	u.returnType = "UPDATED_NEW"
	output, err := u.run(ctx)
	if err != nil {
		return err
	}
	return attributevalue.UnmarshalMap(output.Attributes, out)
}

// Add adds value to path.
// Path can be a number or a set.
// If path represents a number, value is atomically added to the number.
// If path represents a set, value must be a slice, a map[*]struct{}, or map[*]bool.
// Path must be a top-level attribute.
func (u *Update) Add(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	vsub, err := u.subValue(value, flagSet)
	u.setError(err)
	u.add[path] = vsub
	return u
}

// AddStringsToSet adds the given values to the string set specified by path.
func (u *Update) AddStringsToSet(path string, values ...string) *Update {
	return u.Add(path, values)
}

// AddIntsToSet adds the given values to the number set specified by path.
func (u *Update) AddIntsToSet(path string, values ...int) *Update {
	return u.Add(path, values)
}

// AddFloatsToSet adds the given values to the number set specified by path.
func (u *Update) AddFloatsToSet(path string, values ...float64) *Update {
	return u.Add(path, values)
}

func (u *Update) delete(path string, value interface{}) *Update {
	path, err := u.escape(path)
	u.setError(err)
	vsub, err := u.subValue(value, flagSet)
	u.setError(err)
	u.del[path] = vsub
	return u
}

// DeleteFromSet deletes value from the set given by path.
// If value marshals to a set, those values will be deleted.
// If value marshals to a number, string, or binary, that value will be deleted.
// Delete is only for deleting values from sets. See Remove for removing entire attributes.
func (u *Update) DeleteFromSet(path string, value interface{}) *Update {
	v, err := attributevalue.Marshal(value)
	if err != nil {
		u.setError(err)
		return u
	}
	return u.delete(path, v)
}

// DeleteStringsFromSet deletes the given values from the string set specified by path.
func (u *Update) DeleteStringsFromSet(path string, values ...string) *Update {
	return u.delete(path, values)
}

// DeleteIntsFromSet deletes the given values from the number set specified by path.
func (u *Update) DeleteIntsFromSet(path string, values ...int) *Update {
	return u.delete(path, values)
}

// DeleteFloatsFromSet deletes the given values from the number set specified by path.
func (u *Update) DeleteFloatsFromSet(path string, values ...float64) *Update {
	return u.delete(path, values)
}

func (u *Update) Run(ctx context.Context) (*dynamodb.UpdateItemOutput, error) {
	return u.RunWithContext(ctx)
}

func (u *Update) RunWithContext(ctx context.Context) (*dynamodb.UpdateItemOutput, error) {
	return u.run(ctx)
}

func (u *Update) run(ctx context.Context) (*dynamodb.UpdateItemOutput, error) {
	if u.err != nil {
		return nil, u.err
	}
	output := u.updateInput(ctx)
	if u.cc != nil {
		addConsumedCapacity(u.cc, output.ConsumedCapacity)
	}
	return output, u.err
}

func (u *Update) updateInput(ctx context.Context) *dynamodb.UpdateItemOutput {
	input := &dynamodb.UpdateItemInput{
		TableName:                 &u.table.name,
		Key:                       u.key(),
		UpdateExpression:          u.updateExpr(),
		ExpressionAttributeNames:  u.nameExpr,
		ExpressionAttributeValues: u.valueExpr,
		ReturnValues:              u.returnType,
	}
	if u.condition != "" {
		input.ConditionExpression = &u.condition
	}
	if u.cc != nil {
		input.ReturnConsumedCapacity = types.ReturnConsumedCapacityIndexes
	}
	var output *dynamodb.UpdateItemOutput
	output, err := updateItem(ctx, &u.table.db.client, input)
	if err != nil {
		u.err = err
	}
	return output
}

func (u *Update) key() map[string]types.AttributeValue {
	key := map[string]types.AttributeValue{
		u.hashKey: u.hashValue,
	}
	if u.rangeKey != "" {
		key[u.rangeKey] = u.rangeValue
	}
	return key
}

func (u *Update) updateExpr() *string {
	var expr []string
	if len(u.set) > 0 {
		expr = append(expr, "SET", strings.Join(u.set, ", "))
	}
	adds := make([]string, 0, len(u.add))
	for k, v := range u.add {
		adds = append(adds, fmt.Sprintf("%s %s", k, v))
	}
	if len(adds) > 0 {
		expr = append(expr, "ADD", strings.Join(adds, ", "))
	}
	dels := make([]string, 0, len(u.del))
	for k, v := range u.del {
		dels = append(dels, fmt.Sprintf("%s %s", k, v))
	}
	if len(dels) > 0 {
		expr = append(expr, "DELETE", strings.Join(dels, ", "))
	}

	rems := make([]string, 0, len(u.remove))
	for k := range u.remove {
		rems = append(rems, k)
	}
	if len(rems) > 0 {
		expr = append(expr, "REMOVE", strings.Join(rems, ", "))
	}
	joined := strings.Join(expr, " ")
	return &joined
}

func (u *Update) setError(err error) {
	if u.err == nil {
		u.err = err
	}
}
