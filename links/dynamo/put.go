package dynamo

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"log"
)

type Put struct {
	table      ClientTable
	returnType types.ReturnValue

	item map[string]types.AttributeValue
	subber

	condition string
	err       error

	cc *ConsumedCapacity
}

type PutItemAPI interface {
	PutItem(ctx context.Context,
		params *dynamodb.PutItemInput,
		optFns ...func(*dynamodb.Options)) (*dynamodb.PutItemOutput, error)
}

func putItem(ctx context.Context, api PutItemAPI, input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return api.PutItem(ctx, input)
}

func (table ClientTable) Put(item interface{}) *Put {
	encoded, err := attributevalue.MarshalMap(item)
	return &Put{
		table: table,
		item:  encoded,
		err:   err,
	}
}

func (p *Put) ConsumedCapacity(cc *ConsumedCapacity) *Put {
	p.cc = cc
	return p
}

func (p *Put) Run(ctx context.Context) error {
	return p.RunWithContext(ctx)
}

func (p *Put) RunWithContext(ctx context.Context) error {
	p.returnType = "NONE"
	_, err := p.run(ctx)
	return err
}

func (p *Put) run(ctx context.Context) (output *dynamodb.PutItemOutput, err error) {
	if p.err != nil {
		return nil, p.err
	}
	p.input(ctx)
	if p.cc != nil {
		addConsumedCapacity(p.cc, output.ConsumedCapacity)
	}
	return
}

func (p *Put) input(ctx context.Context) *dynamodb.PutItemInput {
	input := &dynamodb.PutItemInput{
		TableName:                 &p.table.name,
		Item:                      p.item,
		ReturnValues:              p.returnType,
		ExpressionAttributeNames:  p.nameExpr,
		ExpressionAttributeValues: p.valueExpr,
	}
	if p.condition != "" {
		input.ConditionExpression = &p.condition
	}
	if p.cc != nil {
		input.ReturnConsumedCapacity = types.ReturnConsumedCapacityIndexes
	}
	_, err := putItem(ctx, &p.table.db.client, input)
	if err != nil {
		log.Printf("Dynamo Put Error ----> %s ", err.Error())
		p.err = err
	}
	return input
}
