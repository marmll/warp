package dynamo

import (
	"bytes"
	"encoding"
	"encoding/base32"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"gitlab.com/marmll/warp/links/dynamo/internal/exprs"
	"strconv"
	"strings"
)

type subber struct {
	nameExpr  map[string]string
	valueExpr map[string]types.AttributeValue
}

func (s *subber) subName(name string) string {
	if s.nameExpr == nil {
		s.nameExpr = make(map[string]string)
	}
	sub := "#s" + encodeName(name)
	s.nameExpr[sub] = name
	return sub
}

func (s *subber) subValue(value interface{}, flags encodeFlags) (string, error) {
	if s.valueExpr == nil {
		s.valueExpr = make(map[string]types.AttributeValue)
	}
	sub := fmt.Sprintf(":v%d", len(s.valueExpr))
	av, err := attributevalue.Marshal(value)
	if err != nil {
		return "", err
	}
	if av == nil {
		return "", fmt.Errorf("invalid substitute value for '%s': %v", sub, av)
	}
	s.valueExpr[sub] = av
	return sub, nil
}

// encodeName consistently encodes a name.
// The consistency is important.
func encodeName(name string) string {
	name = base32.StdEncoding.EncodeToString([]byte(name))
	return strings.TrimRight(name, "=")
}

func (s *subber) escape(name string) (string, error) {
	if upper := strings.ToUpper(name); reserved[upper] {
		return s.subName(name), nil
	}
	// needs to be parsed
	if strings.ContainsAny(name, ".[]()'") {
		return s.subExpr(name, nil)
	}
	// boring
	return name, nil
}

func (s *subber) subExpr(expr string, args ...interface{}) (string, error) {
	return s.subExprFlags(flagNone, expr, args...)
}

func (s *subber) subExprN(expr string, args ...interface{}) (string, error) {
	return s.subExprFlags(flagAllowEmpty|flagNull, expr, args...)
}

func (s *subber) subExprFlags(flags encodeFlags, expr string, args ...interface{}) (string, error) {
	lexed, err := exprs.Parse(expr)
	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	var idx int
	for _, item := range lexed.Items {
		var err error
		switch item.Type {
		case exprs.ItemText:
			_, err = buf.WriteString(item.Val)
		case exprs.ItemQuotedName:
			sub := s.subName(item.Val[1 : len(item.Val)-1]) // trim ""
			_, err = buf.WriteString(sub)
		case exprs.ItemNamePlaceholder:
			switch x := args[idx].(type) {
			case encoding.TextMarshaler:
				var txt []byte
				txt, err = x.MarshalText()
				if err == nil {
					sub := s.subName(string(txt))
					_, err = buf.WriteString(sub)
				}
			case string:
				sub := s.subName(x)
				_, err = buf.WriteString(sub)
			case int:
				_, err = buf.WriteString(strconv.Itoa(x))
			case int64:
				_, err = buf.WriteString(strconv.FormatInt(x, 10))
			default:
				err = fmt.Errorf("dynamo: type of argument for $ must be string, int, or int64 (got %T)", x)
			}
			idx++
		case exprs.ItemValuePlaceholder:
			var sub string
			if sub, err = s.subValue(args[idx], flags); err == nil {
				_, err = buf.WriteString(sub)
			}
			idx++
		case exprs.ItemMagicLiteral:
			_, err = buf.WriteString(args[idx].(string))
			idx++
		}
		if err != nil {
			return "", err
		}
	}
	return buf.String(), nil
}

// wrapExpr wraps expr in parens if needed
func wrapExpr(expr string) string {
	if len(expr) == 0 {
		return expr
	}

	wrap := "(" + expr + ")"

	if !strings.ContainsAny(expr, "()") {
		return wrap
	}

	stack := make([]rune, 0, len(wrap))
	pop := func() rune {
		if len(stack) == 0 {
			return -1
		}
		popped := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		return popped
	}
	for _, r := range wrap {
		if r == ')' {
			var n int
			for r != '(' {
				r = pop()
				if r == -1 {
					// unbalanced expr
					return expr
				}
				n++
			}
			if n <= 1 {
				// redundant parenthesis detected
				return expr
			}
			continue
		}
		stack = append(stack, r)
	}
	return wrap
}
