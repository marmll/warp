package links

import (
	"gitlab.com/marmll/warp/constructors"
	"go.uber.org/fx"
)

func SSMFxOptions() fx.Option {
	return fx.Provide(constructors.SSMService)
}
