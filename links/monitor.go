package links

import (
	"gitlab.com/marmll/warp/constructors"
	"gitlab.com/marmll/warp/links/groups"
	"gitlab.com/marmll/warp/middleware/interceptors/server"
	"go.uber.org/fx"
)

// TODO Revisit Monitoring

// MonitorFxOption adds default metric client to the graph
func MonitorFxOption() fx.Option {
	return fx.Provide(constructors.DefaultMonitor)
}

// MonitorGRPCInterceptorFxOption adds Unary Server Interceptor that will notify metric provider of every call
func MonitorGRPCInterceptorFxOption() fx.Option {
	return fx.Provide(
		fx.Annotated{
			Group:  groups.UnaryServerInterceptors,
			Target: server.MonitorGRPCInterceptor,
		})
}
