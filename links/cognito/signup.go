package cognito

import (
	"context"
	cognito "github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider/types"
	"net/mail"
)

type signUpAPI interface {
	SignUp(ctx context.Context,
		params *cognito.SignUpInput,
		optFns ...func(options *cognito.Options)) (*cognito.SignUpOutput, error)
}

func signUp(c context.Context, api signUpAPI, input *cognito.SignUpInput) (*cognito.SignUpOutput, error) {
	return api.SignUp(c, input)
}

func SignUp(client *cognito.Client, clientId string, username string, password string) (result *cognito.SignUpOutput, err error) {
	var signUpResponse *cognito.SignUpOutput
	var e string
	if valid(username) {
		e = "email"
	} else {
		e = "phone_number"
	}
	attr := []types.AttributeType{{Name: &e, Value: &username}}
	input := &cognito.SignUpInput{
		ClientId:       &clientId,
		Username:       &username,
		Password:       &password,
		UserAttributes: attr,
	}
	if signUpResponse, err = signUp(context.TODO(), client, input); err != nil {
		return nil, err
	}
	return signUpResponse, nil
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
