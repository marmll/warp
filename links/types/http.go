package types

import "gitlab.com/marmll/warp/constructors/partial"

// HTTPHandlerPatternPair defines pattern -> handler pair
//
// Use this type to provide new Internal HTTP Handler
type HTTPHandlerPatternPair = partial.HTTPHandlerPatternPair

// HTTPHandlerFuncPatternPair defines patter -> handler func pair
//
// Use this type to provide new Internal HTTP Handler
type HTTPHandlerFuncPatternPair = partial.HTTPHandlerFuncPatternPair
