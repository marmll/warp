package links

import (
	"gitlab.com/marmll/warp/constructors"
	"go.uber.org/fx"
)

func RedisFxOptions() fx.Option {
	return fx.Provide(constructors.RedisService)
}
