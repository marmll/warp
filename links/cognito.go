package links

import (
	"gitlab.com/marmll/warp/constructors"
	"go.uber.org/fx"
)

func CognitoFxOptions() fx.Option {
	return fx.Provide(constructors.CognitoService)
}
