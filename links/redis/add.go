package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
)

// The normal add function to redis
func RedisSet(ctx context.Context, rdb *redis.Client, key string, value string) error {
	return rdb.Set(ctx, key, value, 0).Err()
}
