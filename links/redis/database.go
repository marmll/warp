package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
)

func RedisFlushAll(ctx context.Context, rdb *redis.Client) error {
	rdb.FlushAll(ctx)
	return nil
}

// function to flush the db --> flushdb
// function to swap databases
