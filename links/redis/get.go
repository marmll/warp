package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
)

// The normal get function to redis
func RedisGet(ctx context.Context, rdb *redis.Client, key string) (string, error) {

	value, err := rdb.Get(ctx, key).Result()

	if err == redis.Nil {
		return "", err
	} else if err != nil {
		return "", err
	}

	return value, nil
}
